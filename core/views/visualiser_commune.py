from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View

from core.models import Commune, Departement


class VisualiserCommune(View):

    def post(self, request):
        nom = request.POST['nom']
        annee_de_creation = request.POST['annee_de_creation']
        code_postal = request.POST['code_postal']
        departement = Departement.objects.get(code_departement=request.POST['departement'])
        type_commune = request.POST['type_commune']
        Commune.objects.create(nom=nom,
                               annee_de_creation=annee_de_creation,
                               code_postal=code_postal,
                               departement=departement,
                               type_commune=type_commune
                               )
        return HttpResponseRedirect("/")

    def get(self, request):
        communes = Commune.objects.all()
        return render(request, "visualiser_communes.html",
                      {"mes_communes": communes})


def my_view(request):
    if request.method == "POST":
        # do something
        return HttpResponseRedirect("/api/visualiser-communes/")
    else:
        # do something else
        return render(request, "visualiser_communes.html", {"ma_commune": "Paris", "nombre_communes": 1})
