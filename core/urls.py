from django.urls import path

from . import views

urlpatterns = [
    path("", views.VisualiserCommune.as_view())
]